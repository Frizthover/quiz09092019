using System;

namespace QUIZ09092019
{
    class BangunDatar
    {
        public void LuasPersegi()
        {int sisi,luas;
            Console.WriteLine("");

            Console.WriteLine("Menghitung Luas Persegi");
            Console.WriteLine("-----------------------");

            Console.WriteLine("Masukkan Nilai Sisi:  ");
            sisi = Convert.ToInt32(Console.ReadLine());

            luas = sisi * sisi;

            Console.WriteLine("Luas Persegi adalah "+luas);
        }

        public void LuasSegitiga()
        {
            int alas,tinggi,luas;
            Console.WriteLine("");

            Console.WriteLine("Menghitung Luas Segitiga");
            Console.WriteLine("------------------------");

            Console.WriteLine("Masukkan Nilai Alas:  ");
            alas= Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Masukkan Nilai Tinggi:  ");
            tinggi= Convert.ToInt32(Console.ReadLine());

            luas= alas*tinggi/2;

            Console.WriteLine("Luas Segitiga adalah "+luas);
        }


        public void LuasLingkaran()
        {
            int r,luas;
            Console.WriteLine("");

            Console.WriteLine("Menghitung Luas Lingkaran");
            Console.WriteLine("-------------------------");

            Console.Write("Masukkan Nilai Jari-Jari:  ");
            r= Convert.ToInt32(Console.ReadLine());

            luas= r*r*22/7;

            Console.WriteLine("Luas Lingkaran adalah "+luas);
        }
    }
}