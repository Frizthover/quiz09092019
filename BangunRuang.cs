using System;

namespace QUIZ09092019
{
    class BangunRuang
    {
        public void VolumeBalok()
        {
        int panjang,lebar,tinggi,volume;

            Console.WriteLine("");

            Console.WriteLine("Menghitung Volume Balok");
            Console.WriteLine("-----------------------");

            Console.WriteLine("Masukkan nilai panjang: ");
            panjang = Convert.ToInt32(Console.ReadLine());

            Console.Write("Masukkan nilai lebar: ");
            lebar = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Masukkan nilai tinggi: ");
            tinggi = Convert.ToInt32(Console.ReadLine());

            volume = panjang * lebar * tinggi;

             Console.WriteLine("Volume Balok adalah "+volume);
        }
    
        public void VolumeKubus()
        {
            int sisi,volume;
            Console.WriteLine("");

            Console.WriteLine("Menghitung Volume Kubus");
            Console.WriteLine("-----------------------");

            Console.WriteLine("Masukkan Nilai Sisi:  ");
            sisi = Convert.ToInt32(Console.ReadLine());

            volume = sisi*sisi*sisi;

             Console.WriteLine("Nilai Volume Kubus adalah "+volume);
        }
    }
}